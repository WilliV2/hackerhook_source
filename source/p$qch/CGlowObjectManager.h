#pragma once

#include "SqchSDK\Interfaces.h"
#include "EntityStructs.h"
#include "Utilities.h"
#include "OptionsManager.h"

class CGlowObjectManager {
public:
	class GlowObjectDefinition_t {
	public:
		void Set(SSDK::Color color) {
			m_vGlowColor = SSDK::Vector(color.rBase(), color.gBase(), color.bBase());
			m_flGlowAlpha = color.aBase();
			m_bRenderWhenOccluded = true;
			m_bRenderWhenUnoccluded = false;
			m_bFullBloomRender = false;
			m_flBloomAmount = 1.f;
		}

		CBaseEntity* GetEntity() {
			return m_hEntity;
		}

		bool IsEmpty() const { return m_nNextFreeSlot != GlowObjectDefinition_t::ENTRY_IN_USE; }

	public:
		CBaseEntity*		m_hEntity;
		SSDK::Vector		m_vGlowColor;
		float				m_flGlowAlpha;

		char				unknown[4];
		float				flUnk;
		float				m_flBloomAmount;
		float				localplayeriszeropoint3;


		bool				m_bRenderWhenOccluded;
		bool				m_bRenderWhenUnoccluded;
		bool				m_bFullBloomRender;
		char				unknown1[1];


		int					m_nFullBloomStencilTestValue;
		int					iUnk;
		int					m_nSplitScreenSlot;
		int					m_nNextFreeSlot;

		static const int END_OF_FREE_LIST = -1;
		static const int ENTRY_IN_USE = -2;
	};

	GlowObjectDefinition_t* m_GlowObjectDefinitions;
	int		max_size;
	int		pad;
	int		size;
	GlowObjectDefinition_t* m_GlowObjectDefinitions2;
	int		currentObjects;
};

void glow()
{
	CGlowObjectManager* pGlowObjectManager = (CGlowObjectManager*)SSDK::O::GlowManager();

	for (int i = 0; i < pGlowObjectManager->size; ++i)
	{
		if (pGlowObjectManager->m_GlowObjectDefinitions[i].IsEmpty() || !pGlowObjectManager->m_GlowObjectDefinitions[i].GetEntity())
			continue;

		CGlowObjectManager::GlowObjectDefinition_t* glowEnt = &pGlowObjectManager->m_GlowObjectDefinitions[i];

		switch (glowEnt->GetEntity()->GetClientClass()->m_ClassID)
		{
		default:
			glowEnt->Set(SSDK::Color(1.0f, 1.0f, 0.0f, 0.8f));
			break;
		case 35:
			glowEnt->Set(SSDK::Color(0.89019f, 0.23137f, 0.23137f, 0.8f));
			break;
		}
	}
}
