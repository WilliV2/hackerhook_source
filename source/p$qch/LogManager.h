#pragma once

#include "DrawManager.h"

struct AppLog
{
	AppLog(std::string text,
		SSDK::Color color,
		float time)
	{
		this->text = text;
		this->color = color;
		this->time = time;
	}

	std::string text;
	SSDK::Color color;
	float time;
};