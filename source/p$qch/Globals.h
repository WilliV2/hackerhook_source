#pragma once

#include "SqchSDK\Interfaces.h"
#include "SqchSDK\Offsets.h"

#include "EntityStructs.h"
#include "OptionsManager.h"

#include "Utilities.h"
#include <deque>
#include "LogManager.h"

namespace Globals
{
	extern bool body_updated;

	extern SSDK::QAngle fake_angles;
	extern SSDK::QAngle real_angles;

	extern int shots_fired;
	extern int hit_shots;
	extern int missed_shots;

	extern float tick_base;

	extern std::deque<AppLog> events;
}