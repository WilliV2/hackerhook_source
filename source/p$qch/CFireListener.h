#pragma once

#include "SqchSDK\Interfaces.h"
#include "SqchSDK\Offsets.h"

#include "EntityStructs.h"
#include "OptionsManager.h"
#include "Globals.h"

#include "Utilities.h"
#include "DrawManager.h"

class CFireListener
{
	class FireListener : IGameEventListener2
	{
	public:
		void start()
		{
			if (!SSDK::I::GameEvents()->AddListener(this, "weapon_fire", false))
			{
				SSDK::I::CVar()->ConsoleColorPrintf(SSDK::Color(255, 0, 0), XorStr("Failed to register weapon fire listener. \n"));
			}
		}
		void stop()
		{
			SSDK::I::GameEvents()->RemoveListener(this);
		}
		void FireGameEvent(IGameEvent* event) override
		{
			CFireListener::singleton()->OnFireEvent(event);
		}
		int GetEventDebugID(void) override
		{
			return EVENT_DEBUG_ID_INIT /*0x2A*/;
		}
	};
public:
	static CFireListener* singleton()
	{
		static CFireListener* instance = new CFireListener();
		return instance;
	}

	void init()
	{
		_listener.start();
	}

	void OnFireEvent(IGameEvent* event)
	{
		if (!strcmp(event->GetName(), "weapon_fire"))
		{
			auto index = SSDK::I::Engine()->GetPlayerUserID(event->GetInt("userid"));

			if (index != SSDK::I::Engine()->GetLocalPlayer())
				return;

			auto local = (CBaseEntity*)SSDK::I::EntityList()->GetClientEntity(index);

			if (!local)
				return;

			Globals::shots_fired++;

			// weapon fire called

	//		std::stringstream string;
	//		string << "Shots fired increased for a total of " << Globals::shots_fired;
	//		Globals::events.push_front(AppLog(string.str(), SSDK::Color(220, 220, 220, 180), SSDK::I::GlobalVars()->curtime));
		}
	}

private:
	FireListener _listener;

};