#include "Utilities.h"

#include <math.h>
#include "Globals.h"

#define M_RADPI 57.295779513082f

bool Utilities::W2S( const SSDK::Vector& v3D, SSDK::Vector& v2D ) {
	return ( SSDK::I::DebugOverlay( )->ScreenPosition( v3D, v2D ) != 1 );
}

SSDK::Vector Utilities::GetEntityBone( CBaseEntity* pEntity, int iBone ) {
	SSDK::matrix3x4_t boneMatrix[ 128 ];

	if ( !pEntity->SetupBones( boneMatrix, 128, 256, SSDK::I::Engine( )->GetLastTimeStamp( ) ) )
		return SSDK::Vector( );

	SSDK::matrix3x4_t hitbox = boneMatrix[ iBone ];

}

float Utilities::calc_tickbase( SSDK::CUserCmd* cmd, CBaseEntity* local )
{
	static int tick = 0;
	static SSDK::CUserCmd* last_cmd = nullptr;

	auto player_tickbase = *local->GetTickBase( );

	if ( !last_cmd || last_cmd->hasbeenpredicted )
		tick = player_tickbase;
	else
		tick++;

	last_cmd = cmd;
	return ( tick * SSDK::I::GlobalVars( )->interval_per_tick );
}

void Utilities::VectorTransform( SSDK::Vector& in1, SSDK::matrix3x4a_t& in2, SSDK::Vector &out ) {
	out.x = in1.Dot( in2.m_flMatVal[ 0 ] ) + in2.m_flMatVal[ 0 ][ 3 ];
	out.y = in1.Dot( in2.m_flMatVal[ 1 ] ) + in2.m_flMatVal[ 1 ][ 3 ];
	out.z = in1.Dot( in2.m_flMatVal[ 2 ] ) + in2.m_flMatVal[ 2 ][ 3 ];
}

float Utilities::interp_fix( )
{
	static SSDK::ConVar* cvar_cl_interp = SSDK::I::CVar( )->FindVar( "cl_interp" );
	static SSDK::ConVar* cvar_cl_updaterate = SSDK::I::CVar( )->FindVar( "cl_updaterate" );
	static SSDK::ConVar* cvar_sv_maxupdaterate = SSDK::I::CVar( )->FindVar( "sv_maxupdaterate" );
	static SSDK::ConVar* cvar_sv_minupdaterate = SSDK::I::CVar( )->FindVar( "sv_minupdaterate" );
	static SSDK::ConVar* cvar_cl_interp_ratio = SSDK::I::CVar( )->FindVar( "cl_interp_ratio" );

	float cl_interp = cvar_cl_interp->GetFloat( );
	int cl_updaterate = cvar_cl_updaterate->GetInt( );
	int sv_maxupdaterate = cvar_sv_maxupdaterate->GetInt( );
	int sv_minupdaterate = cvar_sv_minupdaterate->GetInt( );
	int cl_interp_ratio = cvar_cl_interp_ratio->GetInt( );

	if ( sv_maxupdaterate <= cl_updaterate )
		cl_updaterate = sv_maxupdaterate;

	if ( sv_minupdaterate > cl_updaterate )
		cl_updaterate = sv_minupdaterate;

	float new_interp = ( float ) cl_interp_ratio / ( float ) cl_updaterate;

	if ( new_interp > cl_interp )
		cl_interp = new_interp;

	return max( cl_interp, cl_interp_ratio / cl_updaterate );
}

// if I see one more person use fucking rand( ) %, i am going to fucking shoot myself
float Utilities::random_float( float min, float max )
{
	static std::random_device rd;
	static std::mt19937 gen( rd( ) );
	std::uniform_real_distribution<> dis( min, max );

	return dis( gen );
}

float Utilities::GetFov( const SSDK::QAngle& va, const SSDK::QAngle& aim_a )
{
	SSDK::Vector view, aim;

	AngleVectors( va, view );
	AngleVectors( aim_a, aim );

	return RAD2DEG( acos( aim.Dot( view ) / aim.LengthSqr( ) ) );
}

void Utilities::SinCos( float radians, float *sine, float *cosine ) {
	*sine = sin( radians );
	*cosine = cos( radians );
}

void Utilities::AngleVectors( const SSDK::QAngle &angles, SSDK::Vector *forward ) {
	float sp, sy, cp, cy;

	SinCos( DEG2RAD( angles[ SSDK::MathThings::YAW ] ), &sy, &cy );
	SinCos( DEG2RAD( angles[ SSDK::MathThings::PITCH ] ), &sp, &cp );

	forward->x = cp*cy;
	forward->y = cp*sy;
	forward->z = -sp;
}

void Utilities::AngleVectors( const SSDK::QAngle& angles, SSDK::Vector& forward ) {
	float sp, sy, cp, cy;

	SinCos( DEG2RAD( angles[ SSDK::MathThings::YAW ] ), &sy, &cy );
	SinCos( DEG2RAD( angles[ SSDK::MathThings::PITCH ] ), &sp, &cp );

	forward.x = cp*cy;
	forward.y = cp*sy;
	forward.z = -sp;
}

void Utilities::marquee( std::string& panicova_zlomena_noha )
{
	std::string temp_string = panicova_zlomena_noha;
	panicova_zlomena_noha.erase( 0, 1 );
	panicova_zlomena_noha += temp_string[ 0 ];
}

void Utilities::AngleVectors( const SSDK::QAngle &angles, SSDK::Vector *forward, SSDK::Vector *right, SSDK::Vector *up ) {
	float sr, sp, sy, cr, cp, cy;

	SinCos( DEG2RAD( angles[ SSDK::MathThings::YAW ] ), &sy, &cy );
	SinCos( DEG2RAD( angles[ SSDK::MathThings::PITCH ] ), &sp, &cp );
	SinCos( DEG2RAD( angles[ SSDK::MathThings::ROLL ] ), &sr, &cr );

	if ( forward )
	{
		forward->x = cp*cy;
		forward->y = cp*sy;
		forward->z = -sp;
	}

	if ( right )
	{
		right->x = ( -1 * sr*sp*cy + -1 * cr*-sy );
		right->y = ( -1 * sr*sp*sy + -1 * cr*cy );
		right->z = -1 * sr*cp;
	}

	if ( up )
	{
		up->x = ( cr*sp*cy + -sr*-sy );
		up->y = ( cr*sp*sy + -sr*cy );
		up->z = cr*cp;
	}
}

void Utilities::VectorAngles1( const SSDK::Vector& forward, SSDK::QAngle &angles ) {
	float	tmp, yaw, pitch;

	if ( forward[ 1 ] == 0 && forward[ 0 ] == 0 )
	{
		yaw = 0;
		if ( forward[ 2 ] > 0 )
			pitch = 270;
		else
			pitch = 90;
	}
	else
	{
		yaw = ( atan2( forward[ 1 ], forward[ 0 ] ) * 180 / M_PI );
		if ( yaw < 0 )
			yaw += 360;

		tmp = sqrt( forward[ 0 ] * forward[ 0 ] + forward[ 1 ] * forward[ 1 ] );
		pitch = ( atan2( -forward[ 2 ], tmp ) * 180 / M_PI );
		if ( pitch < 0 )
			pitch += 360;
	}

	angles[ 0 ] = pitch;
	angles[ 1 ] = yaw;
	angles[ 2 ] = 0;
}

void Utilities::VectorAngles( const SSDK::Vector& forward, SSDK::QAngle &angles ) {
	if ( forward[ 1 ] == 0.0f && forward[ 0 ] == 0.0f )
	{
		angles[ 0 ] = ( forward[ 2 ] > 0.0f ) ? 270.0f : 90.0f;
		angles[ 1 ] = 0.0f;
	}
	else
	{
		angles[ 0 ] = atan2( -forward[ 2 ], forward.Length2D( ) ) * -180 / M_PI;
		angles[ 1 ] = atan2( forward[ 1 ], forward[ 0 ] ) * 180 / M_PI;

		if ( angles[ 1 ] > 90 ) angles[ 1 ] -= 180;
		else if ( angles[ 1 ] < 90 ) angles[ 1 ] += 180;
		else if ( angles[ 1 ] == 90 ) angles[ 1 ] = 0;
	}

	angles[ 2 ] = 0.0f;
}

float Utilities::VectorDistance( SSDK::Vector in1, SSDK::Vector in2 )
{
	return sqrtf( pow( in1.x - in2.x, 2 ) + pow( in1.y - in2.y, 2 ) + pow( in1.z - in2.z, 2 ) );
}

SSDK::QAngle Utilities::CalcAngle( SSDK::Vector src, SSDK::Vector dst ) {
	SSDK::QAngle angles;
	SSDK::Vector delta = src - dst;
	VectorAngles( delta, angles );
	Normalize( delta );
	return angles;
}

SSDK::QAngle Utilities::calc_angle( SSDK::Vector src, SSDK::Vector dst )
{
	SSDK::Vector delta = src - dst;

	float mag = sqrtf( delta.x * delta.x + delta.y * delta.y );
	float pitch = atan2f( -delta.z, mag ) * M_RADPI;
	float yaw = atan2f( delta.y, delta.x ) * M_RADPI;

	SSDK::QAngle angle( pitch, yaw, 0.f );
	Normalize( angle );
	return angle;
}

void Utilities::CalcAngleOutput( SSDK::Vector src, SSDK::Vector dst, SSDK::QAngle output )
{
	/* ~~ good old pythag ~~ */
	SSDK::Vector delta = src - dst;
	double hypotenuse = delta.Length2D( ); // delta.Length
	output.y = ( atan( delta.y / delta.x ) * 57.295779513082f );
	output.x = ( atan( delta.z / hypotenuse ) * 57.295779513082f );
	output[ 2 ] = 0.00;

	if ( delta.x >= 0.0 )
		output.y += 180.0f;
}

bool Utilities::ClampAll( SSDK::QAngle &angles ) {
	SSDK::QAngle a = angles;
	Normalize( a );
	ClampAngles( a );

	if ( isnan( a.x ) || isinf( a.x ) ||
		isnan( a.y ) || isinf( a.y ) ||
		isnan( a.z ) || isinf( a.z ) ) {
		return false;
	}
	else {
		angles = a;
		return true;
	}
}

void Utilities::Normalize( SSDK::QAngle& angle ) {
	while ( angle.x > 89.0f ) {
		angle.x -= 180.f;
	}
	while ( angle.x < -89.0f ) {
		angle.x += 180.f;
	}
	while ( angle.y > 180.f ) {
		angle.y -= 360.f;
	}
	while ( angle.y < -180.f ) {
		angle.y += 360.f;
	}
}

float Utilities::Normalize( float value )
{
	while ( value > 180 )
		value -= 360.f;

	while ( value < -180 )
		value += 360.f;
	return value;
}

void Utilities::ClampAngles( SSDK::QAngle &angles ) {
	if ( angles.y > 180.0f )
		angles.y = 180.0f;
	else if ( angles.y < -180.0f )
		angles.y = -180.0f;

	if ( angles.x > 89.0f )
		angles.x = 89.0f;
	else if ( angles.x < -89.0f )
		angles.x = -89.0f;

	angles.z = 0;
}

void Utilities::VectorAngles3D( SSDK::Vector& forward, SSDK::QAngle angles )
{
	SSDK::Vector view;
	if ( forward[ 1 ] == 0.f && forward[ 0 ] == 0.f )
	{
		view[ 0 ] = 0.f;
		view[ 1 ] = 0.f;
	}
	else
	{
		view[ 1 ] = atan2( forward[ 1 ], forward[ 0 ] ) * 180.f / M_PI;

		if ( view[ 1 ] < 0.f )
			view[ 1 ] += 360;

		view[ 2 ] = sqrt( forward[ 0 ] * forward[ 0 ] + forward[ 1 ] * forward[ 1 ] );

		view[ 0 ] = atan2( forward[ 2 ], view[ 2 ] ) * 180.f / M_PI;
	}

	angles[ 0 ] = -view[ 0 ];
	angles[ 1 ] = view[ 1 ];
}

SSDK::Vector Utilities::ExtrapolateTick( SSDK::Vector p0, CBaseEntity* ent ) {
	return p0 + ( *ent->GetVelocity( ) * SSDK::I::GlobalVars( )->interval_per_tick );
}

void Utilities::SetClanTag( const char* tag, const char* name ) {
	using SetClanTagFn = void( __fastcall* ) ( const char *tag, const char *name );
	static SetClanTagFn SetClanTagEx = ( SetClanTagFn ) ( SSDK::O::FindSignature( XorStr( "engine.dll" ), XorStr( "53 56 57 8B DA 8B F9 FF 15" ) ) );
	SetClanTagEx( tag, name );
}

float Utilities::RandomFloat( float min, float max )
{
	typedef float( *RandomFloat_t )( float, float );
	static RandomFloat_t m_RandomFloat = ( RandomFloat_t ) GetProcAddress( GetModuleHandle( XorStr( "vstdlib.dll" ) ), XorStr( "RandomFloat" ) );
	return m_RandomFloat( min, max );
}

void Utilities::RandomSeed( int iSeed )
{
	typedef void( *RandomSeed_t )( int );
	static RandomSeed_t m_RandomSeed = ( RandomSeed_t ) GetProcAddress( GetModuleHandle( XorStr( "vstdlib.dll" ) ), XorStr( "RandomSeed" ) );
	return m_RandomSeed( iSeed );
}

template< class T, class Y >
T Utilities::ClampValue( T const &val, Y const &minVal, Y const &maxVal )
{
	if ( val < minVal )
		return minVal;
	else if ( val > maxVal )
		return maxVal;
	else
		return val;
}

float Utilities::GetInterp( )
{
	float updaterate = SSDK::I::CVar( )->FindVar( XorStr( "cl_updaterate" ) )->GetFloat( );
	SSDK::ConVar* minupdate = SSDK::I::CVar( )->FindVar( XorStr( "sv_minupdaterate" ) );
	SSDK::ConVar* maxupdate = SSDK::I::CVar( )->FindVar( XorStr( "sv_maxupdaterate" ) );

	if ( minupdate && maxupdate )
		updaterate = maxupdate->GetFloat( );

	float ratio = SSDK::I::CVar( )->FindVar( XorStr( "cl_interp_ratio" ) )->GetFloat( );

	if ( ratio == 0 )
		ratio = 1.0f;

	float lerp = SSDK::I::CVar( )->FindVar( XorStr( "cl_interp" ) )->GetFloat( );
	SSDK::ConVar* cmin = SSDK::I::CVar( )->FindVar( XorStr( "sv_client_min_interp_ratio" ) );
	SSDK::ConVar* cmax = SSDK::I::CVar( )->FindVar( XorStr( "sv_client_max_interp_ratio" ) );

	if ( cmin && cmax && cmin->GetFloat( ) != 1 )
		ratio = ClampValue( ratio, cmin->GetFloat( ), cmax->GetFloat( ) );

	return max( lerp, ratio / updaterate );
}

float Utilities::GetNetworkLatency( )
{
	// Get true latency
	SSDK::INetChannelInfo *nci = SSDK::I::Engine( )->GetNetChannelInfo( );
	if ( nci )
	{
		float OutgoingLatency = nci->GetAvgLatency( 0 );
		return OutgoingLatency;
	}
	else
		return 0.0f;
}

float Utilities::GetTraceFractionWorldProps( SSDK::Vector startpos, SSDK::Vector endpos ) {
	SSDK::Ray_t ray;
	SSDK::Trace_t tr;
	SSDK::CTraceFilterWorldOnly filter;

	ray.Init( startpos, endpos );

	SSDK::I::EngineTrace( )->TraceRay( ray, MASK_SHOT, &filter, &tr );

	return tr.fraction;
}

// below is broken hitchance, if you absolutely fucking beg me, i'll tell u how 2 fix.
bool Utilities::HitChance( CBaseEntity* pCSLocalPlayer, CBaseEntity* pCSTarget, CBaseCombatWeapon* pCSWeapon, SSDK::QAngle qAngle, int seed, int chance ) {
	if ( !chance )
		return true;

	int iHit = 0;
	int iHitsNeed = ( int ) ( ( float ) OptionsManager.iHitChanceSeed * ( ( float ) chance / 100.f ) );
	bool bHitchance = false;

	SSDK::Vector forward, right, up;
	AngleVectors( qAngle, &forward, &right, &up );

	pCSWeapon->UpdateAccuracy( );

	for ( auto i = 0; i < OptionsManager.iHitChanceSeed; ++i ) {
		//Works better without seed lel
		//RandomSeed((seed & 0xFF) + 1);

		float RandomA = RandomFloat( 0.0f, 1.0f );
		float RandomB = 1.0f - RandomA * RandomA;

		RandomA = RandomFloat( 0.0f, M_PI_F * 2.0f );
		RandomB *= pCSWeapon->GetInaccuracy( );

		float SpreadX1 = ( cos( RandomA ) * RandomB );
		float SpreadY1 = ( sin( RandomA ) * RandomB );

		float RandomC = RandomFloat( 0.0f, 1.0f );
		float RandomF = RandomF = 1.0f - RandomC * RandomC;

		RandomC = RandomFloat( 0.0f, M_PI_F * 2.0f );
		RandomF *= pCSWeapon->GetSpread( );

		float SpreadX2 = ( cos( RandomC ) * RandomF );
		float SpreadY2 = ( sin( RandomC ) * RandomF );

		float fSpreadX = SpreadX1 + SpreadX2;
		float fSpreadY = SpreadY1 + SpreadY2;

		SSDK::Vector vSpreadForward;
		vSpreadForward[ 0 ] = forward[ 0 ] + ( fSpreadX * right[ 0 ] ) + ( fSpreadY * up[ 0 ] );
		vSpreadForward[ 1 ] = forward[ 1 ] + ( fSpreadX * right[ 1 ] ) + ( fSpreadY * up[ 1 ] );
		vSpreadForward[ 2 ] = forward[ 2 ] + ( fSpreadX * right[ 2 ] ) + ( fSpreadY * up[ 2 ] );
		vSpreadForward.NormalizeInPlace( );

		SSDK::QAngle qaNewAngle;
		VectorAngles1( vSpreadForward, qaNewAngle );
		Utilities::Normalize( qaNewAngle );

		//DEBUG
		//SSDK::I::Engine()->SetViewAngles(qaNewAngle);

		SSDK::QAngle vEnd;
		Utilities::AngleVectors( qaNewAngle, &vEnd );

		vEnd = pCSLocalPlayer->GetEyePos( ) + ( vEnd * 8192.f );

		if ( CBaseEntity::GetLocalPlayer( )->canHit( vEnd, pCSTarget ) ) {
			iHit++;
		}

		if ( ( int ) ( ( ( float ) iHit / OptionsManager.iHitChanceSeed ) * 100.f ) >= chance ) {
			bHitchance = true;
			break;
		}

		if ( ( OptionsManager.iHitChanceSeed - 1 - i + iHit ) < iHitsNeed )
			break;
	}

	return bHitchance;
}

bool Utilities::CircleStrafer( SSDK::CUserCmd* pCmd, SSDK::Vector& vecOriginalView ) {
	CBaseEntity* pLocalEntity = CBaseEntity::GetLocalPlayer( );

	if ( !pLocalEntity->isValidPlayer( true ) )
		return false;

	static int iCircleFact = 0;
	iCircleFact++;
	if ( iCircleFact > 361 )
		iCircleFact = 0;

	float flRotation = 3.f * iCircleFact - SSDK::I::GlobalVars( )->interval_per_tick;

	SSDK::Vector StoredViewAngles = pCmd->viewangles;

	if ( GetAsyncKeyState( VK_MBUTTON ) ) {
		pCmd->forwardmove = 450.f;
		pCmd->viewangles = vecOriginalView;
		flRotation = DEG2RAD( flRotation );

		float cos_rot = cos( flRotation );
		float sin_rot = sin( flRotation );

		float new_forwardmove = ( cos_rot * pCmd->forwardmove ) - ( sin_rot * pCmd->sidemove );
		float new_sidemove = ( sin_rot * pCmd->forwardmove ) + ( cos_rot * pCmd->sidemove );

		pCmd->forwardmove = new_forwardmove;
		pCmd->sidemove = new_sidemove;
	}

	return true;
}


int Utilities::FakelagCompensationBreak( ) {
	CBaseEntity *local = CBaseEntity::GetLocalPlayer( );
	SSDK::Vector velocity = *local->GetVelocity( );
	velocity.z = 0.f;
	float speed = velocity.Length2D( );
	if ( speed > 0.f )
	{
		auto distance_per_tick = speed *
			SSDK::I::GlobalVars( )->interval_per_tick;
		int choked_ticks = std::ceilf( 64.f / distance_per_tick );
		return std::min<int>( choked_ticks, 14 );
	}
	return 1;
}