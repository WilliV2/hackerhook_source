#pragma once

#include "SqchSDK\Interfaces.h"
#include "SqchSDK\Offsets.h"

#include "EntityStructs.h"
#include "OptionsManager.h"

#include "Utilities.h"
#include "Globals.h"
#include <deque>
#include "SqchSDK\SDK\PlayerInfo.h"

//std::string issabadthang[65];

class CResolver
{
public:
	float lastmoving[65][8];
	float lbys[65][8];
	float eyes[65][8];
	float_t ld(CBaseEntity * ent, int i);
	int GetDifferentDeltas(CBaseEntity * ent);
	float GetDeltaByComparingEightTicks(CBaseEntity * ent);
	float GetMostFrequent(int num);
	void SetLBYs(int num, CBaseEntity* ent);
	void SetMLBYs(int num, CBaseEntity * ent);
	void SetEyes(int num, CBaseEntity * ent);
	bool IsFakewalking(CBaseEntity * player);
	void Resolver_Fake_walk(CBaseEntity * entity);
	void Resolver_standing(CBaseEntity *entity);
	void Solve_Yaw();

};