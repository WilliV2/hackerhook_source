#include "DirectX.h"
#include <fstream>
#include "ImGUI\imgui_internal.h"


static const char* sidebar_tabs[] = {
	"Anti-Aim",
	"Visual",
	"Misc",
	"Aim",
};


void DrawRectRainbow(int x, int y, int width, int height, float flSpeed, float &flRainbow)
{
	ImDrawList* windowDrawList = ImGui::GetWindowDrawList();

	SSDK::Color colColor(0, 0, 0, 255);

	flRainbow += flSpeed;
	if (flRainbow > 1.f) flRainbow = 0.f;

	for (int i = 0; i < width; i++)
	{
		float hue = (1.f / (float)width) * i;
		hue -= flRainbow;
		if (hue < 0.f) hue += 1.f;

		SSDK::Color colRainbow = colColor.FromHSB(hue, 1.f, 1.f);
		windowDrawList->AddRectFilled(ImVec2(x + i, y), ImVec2(width, height), colRainbow.GetU32());
	}
}


template<size_t N>
void render_tabs(const char* (&names)[N], int& activetab, float w, float h, bool sameline)
{
	bool values[N] = { false };

	values[activetab] = true;

	for (auto i = 0u; i < N; ++i) {
		if (ImGui::Button(names[i], ImVec2{ w, h })) {
			activetab = i;
		}
		if (sameline && i < N - 1)
			ImGui::SameLine();
	}
}
int get_fps()
{
	using namespace std::chrono;
	static int count = 0;
	static auto last = high_resolution_clock::now();
	auto now = high_resolution_clock::now();
	static int fps = 0;

	count++;

	if (duration_cast<milliseconds>(now - last).count() > 1000) {
		fps = count;
		count = 0;
		last = now;
	}

	return fps;
}



void HvHRenderTab()
{
	auto& style = ImGui::GetStyle();
	static const char* hvh_tabs_names[] = { "AA", "Setts" };
	static int   active_hvh_tab = 0;
	float group_w = ImGui::GetCurrentWindow()->Size.x - style.WindowPadding.x * 2;



	render_tabs(hvh_tabs_names, active_hvh_tab, group_w / _countof(hvh_tabs_names), 25.0f, true);

		
							bool placeholder_true = true;

						
					

							ImGui::BeginGroupBox("##body_content", ImVec2::Zero);
							{
								if (active_hvh_tab == 0) {


									ImGui::Checkbox(XorStr("Enable Anti-Aims"), &OptionsManager.bAA);

									ImGui::Checkbox(XorStr("pitch"), &OptionsManager.iAAPitch);

									ImGui::Checkbox(XorStr("Yaw"), &OptionsManager.iAARealYaw);

									ImGui::Checkbox(XorStr("Fake Yaw"), &OptionsManager.iAAFakeYaw);


									ImGui::Checkbox(XorStr("Fakelag"), &OptionsManager.bFakeLag);


								

								}else if (active_hvh_tab == 1) {
									ImGui::SliderFloat(XorStr("reset air speed"), &OptionsManager.speed_in_air, 1, 200);

									ImGui::SliderFloat(XorStr("reset air range"), &OptionsManager.reset_in_air_range, 1, 200);
									
									ImGui::SliderInt(XorStr("Choke Packets"), &OptionsManager.iFakeLagAmount, 0, 14);

									ImGui::SliderInt(XorStr("Break Delta"), &OptionsManager.iBreakerDelta, 0, 119);
							}
}
							ImGui::EndGroupBox();
					}
void AimbotRenderTab( )
{

	auto& style = ImGui::GetStyle();
	static const char* aim_tabs_names[] = { "Main", "Accuracy","Other" };
	static int   active_aim_tab = 0;
	float group_w = ImGui::GetCurrentWindow()->Size.x - style.WindowPadding.x * 2;
	render_tabs(aim_tabs_names, active_aim_tab, group_w / _countof(aim_tabs_names), 25.0f, true);
	bool placeholder_true = true;

	static const char *szMultiHitboxes[] = {
		XorStr("Head"), XorStr("Neck"), XorStr("Upper Chest"), XorStr("Chest"), XorStr("Thorax"),
		XorStr("Body"), XorStr("Left Forearm"), XorStr("Right Forearm"), XorStr("Hands"), XorStr("Left Thigh"),
		XorStr("Right Thigh"), XorStr("Left Calf"), XorStr("Right Calf"), XorStr("Left Foot"), XorStr("Right Foot")
	};
	static const char *szHitboxes[] = { XorStr("Head"), XorStr("Neck"), XorStr("Chest"), XorStr("Pelvis") };

	ImGui::BeginGroupBox("##body_contenttttt", ImVec2::Zero);
	{
		if (active_aim_tab == 0) {
			ImGui::Checkbox(XorStr("Auto Shoot"), &OptionsManager.bAutoShoot);
			ImGui::Checkbox(XorStr("Auto Pistol"), &OptionsManager.bAutoPistol);
			ImGui::Checkbox(XorStr("Auto Revolver"), &OptionsManager.bAutoRevolver);
			ImGui::Checkbox(XorStr("Auto Scope"), &OptionsManager.bAutoScope);
			ImGui::Checkbox(XorStr("Silent aim"), &OptionsManager.bSilentAim);
		


		

		}
		else if (active_aim_tab == 1)
		{
			ImGui::Checkbox(XorStr("Resolver"), &OptionsManager.bCorrect);
			ImGui::Checkbox(XorStr("Lag Compensation"), &OptionsManager.bInterpLagComp);
			ImGui::SliderInt(XorStr("Hit Chance"), &OptionsManager.iHitchance, 0, 100);
			ImGui::SliderInt(XorStr("Hit Chance Max Seeds"), &OptionsManager.iHitChanceSeed, 0, 256);
			ImGui::SliderFloat(XorStr("Minimum Damage"), &OptionsManager.flMinDmg, 0, 100, "%.2f");
			ImGui::Checkbox(XorStr("Prefer Selected Hitbox"), &OptionsManager.bPrioritize);
			ImGui::Checkbox(XorStr("Multi-Point"), &OptionsManager.bPrioritizeVis);
			ImGui::Combo(XorStr("Selected Hitbox"), &OptionsManager.iHitbox, szHitboxes, ARRAYSIZE(szHitboxes));
			ImGui::SliderInt(XorStr("Point Scale"), &OptionsManager.iPointScale, 0, 100);
			ImGui::Text(XorStr("Hitboxes to Scan: "));
			for (int i = 0; i < ARRAYSIZE(szMultiHitboxes); i++)
			{
				ImGui::Selectable(szMultiHitboxes[i], &OptionsManager.aMultiHitboxes[i]);
			}


		}
		else if (active_aim_tab == 2) {
			ImGui::Checkbox(XorStr("Auto Body-Aim"), &OptionsManager.bAutoBaim);
			ImGui::SliderInt(XorStr("Body-Aim After X Shots"), &OptionsManager.iAutoBaimAferShot, 0, 10);
			static const char *szBaimHitboxes[] = { XorStr("Chest"), XorStr("Body"), XorStr("Pelvis") };
			ImGui::Combo(XorStr("Body Aim Hitbox"), &OptionsManager.iHitboxAutoBaim, szBaimHitboxes, ARRAYSIZE(szBaimHitboxes));
		}


		ImGui::EndGroupBox();




	}
	
}

void MiscRenderTab( )
{
	
	ImGui::Checkbox( XorStr( "No Flash" ), &OptionsManager.bNoFlash );
	ImGui::Checkbox(XorStr("Clantag"), &OptionsManager.bClantagchanger);
	//ImGui::Checkbox(XorStr("Logs"), &OptionsManager.bEventLog);

	ImGui::Spacing( );
	//ImGui::Checkbox(XorStr("Indicators"), &OptionsManager.bIndicators);
}

void VisualsRenderTab()
{
	auto& style = ImGui::GetStyle();
	float group_w = ImGui::GetCurrentWindow()->Size.x - style.WindowPadding.x * 2;

	bool placeholder_true = true;
		



			ImGui::Checkbox(XorStr("Enable ESP"), &OptionsManager.bEspEnabled);
			if (OptionsManager.bEspEnabled) {
				ImGui::Checkbox(XorStr("Box"), &OptionsManager.bBoxESP);
				ImGui::Checkbox(XorStr("Health"), &OptionsManager.bHealthbar);
				ImGui::Checkbox(XorStr("Name"), &OptionsManager.bPlayerName);
				ImGui::Checkbox(XorStr("No Visual Recoil"), &OptionsManager.bNoVisRecoil);
				ImGui::Checkbox(XorStr("No Scope"), &OptionsManager.bNoScope);
				ImGui::Checkbox(XorStr("Crosshair"), &OptionsManager.bCrosshair);
				ImGui::Checkbox("Third Person (x)", &OptionsManager.iThirdPerson);
				ImGui::Checkbox("Bullet Tracers", &OptionsManager.bBulletTracers);

				static const char *szChamsMode[] = { XorStr("Flat"), XorStr("Material"), };
				ImGui::Combo("Chams Type", &OptionsManager.bChamsmode, szChamsMode, ARRAYSIZE(szChamsMode));


				ImGui::SliderInt(XorStr("VFOV"), &OptionsManager.iFov, 68, 130);
				static const char *szIndicator[] = { XorStr("Off"), XorStr("Ghost"), XorStr("Lines"), XorStr("Both") };
				ImGui::Combo(XorStr("info"), &OptionsManager.iIndicators, szIndicator, ARRAYSIZE(szIndicator));

			}

}





constexpr static float get_sidebar_item_width() { return 150.0f; }
constexpr static float get_sidebar_item_height() { return  50.0f; }




ImVec2 get_sidebar_size()
{
	constexpr float padding = 10.0f;
	constexpr auto size_w = padding * 2.0f + get_sidebar_item_width();
	constexpr auto size_h = padding * 2.0f + (sizeof(sidebar_tabs) / sizeof(char*)) * get_sidebar_item_height();

	return ImVec2{ size_w, size_h };
}




namespace DirectX {
	EndScene_t g_fnOriginalEndScene;
	Reset_t g_fnOriginalReset;

	WNDPROC g_pOldWindowProc;
	HWND g_hWindow;

	bool g_IsInitialized = false;

	HRESULT __stdcall Hooked_EndScene( IDirect3DDevice9* pDevice ) {
		/* ~~ imgui start ~~ */

		if ( !g_IsInitialized ) {
			ImGui_ImplDX9_Init( g_hWindow, pDevice );

			ImGuiStyle * style = &ImGui::GetStyle( );
			style->Alpha = 1.f;
			style->AntiAliasedLines = true;
			style->AntiAliasedShapes = true;
			style->FrameRounding = 3.0f;
			style->ItemInnerSpacing = ImVec2(8, 8);
	
			style->Colors[ImGuiCol_Text] = ImVec4(0.90f, 1.00f, 1.00f, 1.00f);
			style->Colors[ImGuiCol_TextDisabled] = ImVec4(0.11f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_WindowBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
			style->Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			style->Colors[ImGuiCol_PopupBg] = ImVec4(0.00f, 0.00f, 0.02f, 1.00f);
			style->Colors[ImGuiCol_Border] = ImVec4(0.12f, 0.11f, 0.21f, 0.00f);
			style->Colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			style->Colors[ImGuiCol_FrameBg] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.09f, 0.09f, 0.12f, 1.00f);
			style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.12f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_TitleBg] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.05f, 0.02f, 0.05f, 0.98f);
			style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.02f);
			style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.09f, 0.09f, 0.12f, 1.00f);
			style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.12f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.18f, 0.18f, 0.33f, 1.00f);
			style->Colors[ImGuiCol_ComboBg] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_CheckMark] = ImVec4(0.12f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.09f, 0.09f, 0.12f, 1.00f);
			style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.11f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_Button] = ImVec4(0.09f, 0.09f, 0.12f, 1.00f);
			style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.12f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.18f, 0.18f, 0.33f, 1.00f);
			style->Colors[ImGuiCol_Header] = ImVec4(0.09f, 0.09f, 0.12f, 1.00f);
			style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.12f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.18f, 0.18f, 0.33f, 1.00f);
			style->Colors[ImGuiCol_CloseButton] = ImVec4(0.12f, 0.11f, 0.21f, 1.00f);
			style->Colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.18f, 0.18f, 0.33f, 1.00f);
			style->Colors[ImGuiCol_CloseButtonActive] = ImVec4(0.18f, 0.18f, 0.33f, 1.00f);
			style->Colors[ImGuiCol_PlotLines] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
			style->Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_PlotHistogram] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.00f, 0.02f, 0.00f, 1.00f);
			style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
			style->Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);

			g_IsInitialized = true;
		}
		else {
			ImGui::GetIO( ).MouseDrawCursor = OptionsManager.bShowMenu;
			ImGui_ImplDX9_NewFrame( );


			if ( OptionsManager.bShowMenu )
			{
				
				ImGui::SetNextWindowPos(ImVec2{ 0, 0 }, ImGuiSetCond_Once);
				ImGui::SetNextWindowSize(ImVec2{ 1000, 0 }, ImGuiSetCond_Once);
				const auto sidebar_size = get_sidebar_size();
				static int active_sidebar_tab = 0;
				if ( ImGui::Begin( " hackerhook v2 Recode", &OptionsManager.bShowMenu, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoResize ) )
				{


					static float flRainbow;
					float flSpeed = 0.0003f;
					int curWidth = 1;
					ImVec2 curPos = ImGui::GetCursorPos();
					ImVec2 curWindowPos = ImGui::GetWindowPos();
					curPos.x += curWindowPos.x;
					curPos.y += curWindowPos.y;
					int size1;
					int y;
					SSDK::I::Engine()->GetScreenSize(y, size1);
					DrawRectRainbow(curPos.x - 10, curPos.y - 5, ImGui::GetWindowSize().x + size1, curPos.y + -4, flSpeed, flRainbow);



					ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2::Zero);
					{
						ImGui::BeginGroupBox("##sidebar", sidebar_size);
						{
							ImGui::GetCurrentWindow()->Flags &= ~ImGuiWindowFlags_ShowBorders;

							render_tabs(sidebar_tabs, active_sidebar_tab, get_sidebar_item_width(), get_sidebar_item_height(), false);
						}
						ImGui::EndGroupBox();
					}
					ImGui::PopStyleVar();
					ImGui::SameLine();


					auto size = ImVec2{ 0.0f, sidebar_size.y };

					ImGui::BeginGroupBox("##body", size);
					if (active_sidebar_tab == 0) {
						HvHRenderTab();
					}
					else if (active_sidebar_tab == 1) {
						VisualsRenderTab();
					}
					else if (active_sidebar_tab == 2) {
						MiscRenderTab();
					}
					else if (active_sidebar_tab == 3) {
						AimbotRenderTab();
					}
					ImGui::EndGroupBox();



					ImGui::TextColored(ImVec4{ 0.0f, 0.5f, 0.0f, 1.0f }, "FPS: %03d", get_fps());
					ImGui::End( );
				}
			}

			ImGui::Render( );
		}

		/* ~~ imgui end ~~ */



		return g_fnOriginalEndScene( pDevice );
	}

	HRESULT __stdcall Hooked_Reset( IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters ) {
		if ( !g_IsInitialized ) return g_fnOriginalReset( pDevice, pPresentationParameters );
		ImGui_ImplDX9_InvalidateDeviceObjects( );

		auto hr = g_fnOriginalReset( pDevice, pPresentationParameters );
		ImGui_ImplDX9_CreateDeviceObjects( );
		return hr;
	}

	LRESULT __stdcall Hooked_WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam ) {
		switch ( uMsg ) {
		case WM_LBUTTONDOWN:
			OptionsManager.vecPressedKeys[ VK_LBUTTON ] = true;
			break;
		case WM_LBUTTONUP:
			OptionsManager.vecPressedKeys[ VK_LBUTTON ] = false;
			break;
		case WM_RBUTTONDOWN:
			OptionsManager.vecPressedKeys[ VK_RBUTTON ] = true;
			break;
		case WM_RBUTTONUP:
			OptionsManager.vecPressedKeys[ VK_RBUTTON ] = false;
			break;
		case WM_KEYDOWN:
			OptionsManager.vecPressedKeys[ wParam ] = true;
			break;
		case WM_KEYUP:
			OptionsManager.vecPressedKeys[ wParam ] = false;
			break;
		default: break;
		}

		static SSDK::ConVar *conMouseEnable = SSDK::I::CVar( )->FindVar( XorStr( "cl_mouseenable" ) );
		static bool isDown = false;
		static bool isClicked = false;
		if ( OptionsManager.vecPressedKeys[ VK_INSERT ] ) {
			isClicked = false;
			isDown = true;
		}
		else if ( !OptionsManager.vecPressedKeys[ VK_INSERT ] && isDown ) {
			isClicked = true;
			isDown = false;
		}
		else {
			isClicked = false;
			isDown = false;
		}

		if ( isClicked ) {
			if ( OptionsManager.bShowMenu ) {
				//CONFIG SAVER - LOAD
				std::ofstream ofs( "hackerhook.config", std::ios::binary );
				ofs.write( ( char* ) &OptionsManager, sizeof( OptionsManager ) );
			}
			OptionsManager.bShowMenu = !OptionsManager.bShowMenu;
			conMouseEnable->SetValue( !OptionsManager.bShowMenu );
		}

		/*	if (OptionsManager.bEdge)
				OptionsManager.bFindAngles = false;

			if (OptionsManager.bFindAngles)
				OptionsManager.bEdge = false;*/


		if ( g_IsInitialized && OptionsManager.bShowMenu && ImGui_ImplDX9_WndProcHandler( hWnd, uMsg, wParam, lParam ) )
			return true;

		return CallWindowProc( g_pOldWindowProc, hWnd, uMsg, wParam, lParam );
	}
}